<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{

    public function index()
    {
        $search = request('search');

        if ($search) {

            $events = Event::where([
                ['title', 'like', '%' . $search . '%']
            ])->paginate(15);

        } else {
            $events = Event::paginate(15);
        }


        return view('welcome', ['events' => $events, 'search' => $search]);
    }

    public function created()
    {
        return view("events.created");
    }

    public function createdo(Request $request)
    {
        try {


            DB::beginTransaction();

            $event = new Event();
            $event->title = $request->title;
            $event->city = $request->city;
            $event->private = $request->private;
            $event->description = $request->description;
            $event->items = $request->items;
            $event->user_id = Auth::user()->id;
            $event->date = $request->date;

            if ($request->hasFile('image') && $request->file('image')->isValid()) {

                //request->file('image')->store("imgs-events");

                $event->image = $request->file('image')->store("imgs-events");
//                Storage::path('file.jpg')
            }

//            $event = Event::create([
//                "title" => $request->title,
//                "city" => $request->city,
//                "private" => $request->private,
//                "description" => $request->description
//            ]);


            $event->save();
            DB::commit();

            return redirect()->route('events')->with('eventoMsg', 'Evento criado com sucesso');

        } catch (\Exception $e) {

            DB::rollBack();
            return redirect()->route('events')->with('eventoMsg', 'Erro ao criar! ' . $e->getMessage());

        }
    }

    public function show(int $id)
    {
        $event = Event::findOrFail($id);

        $hasUserJoined = false;
        $user = Auth::user();
        if ($user) {
            $userEvents = $user->eventsAsParticipant;

            if (count($userEvents) > 0){
                foreach ($userEvents as $userEvent) {
                    if ($userEvent->id == $id){
                        $hasUserJoined = true;
                    }
                }
            }

        }

        $donoEvento = User::where('id', $event->user_id)->first();

        return view('events.detalhar', ['event' => $event, 'dono' => $donoEvento, 'hasUserJoined' => $hasUserJoined]);
    }

    public function dashboard()
    {
        $user = Auth::user();

        $events = $user->events;


        return view('events.dashboard', ['events' => $events, 'eventsasparticipante' => $user->eventsAsParticipant]);
    }

    public function destroy(int $id)
    {
        try {
            DB::beginTransaction();

            $event = Event::findOrFail($id);

            Storage::delete($event->image);

            $event->delete();

            DB::commit();

            return redirect()->route('dashboard')->with('eventoMsg', 'Evento deletado com sucesso');

        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('dashboard')->with('eventoMsg', 'Erro ao deletar evento ' . $e->getMessage());


        }


    }

    public function edit(Event $event)
    {
        if (Auth::user()->id != $event->user_id) {

            return redirect()->route('dashboard')->with('eventoMsg', 'Você pode editar somente seus eventos');

        }

        return view('events.edit', ['event' => $event]);
    }

    public function editDo(Event $event, Request $request)
    {


        $data = $request->all();


        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            Storage::delete($event->image);

            $data["image"] = Storage::put('imgs-events', $request->file('image'));
        }

        $event->update($data);


        return redirect()->route('dashboard')->with('eventoMsg', 'Evento editado com sucesso');


    }

    public function joinEvent(Event $event)
    {
        $user = Auth::user();

        $user->eventsAsParticipant()->attach($event->id);

        return redirect()->route('dashboard')->with('eventoMsg', 'Sua presença foi confirmada: ' . $event->title);


    }

    public function leaveEvent(Event $event)
    {

        Auth::user()->eventsAsParticipant()->detach($event->id);

        return redirect()->route('dashboard')->with('eventoMsg', 'Sua presença foi removida: ' . $event->title);
    }

    public function datasProgramacao(Request $request)
    {



        $events = Event::all('title', 'date', 'id');

        return view('events.programacao', compact('events'));

    }


}
