@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

    <div class="col-md-10 offset-md-1 dashboard-title-container">
        <h1>Eventos criado por {{strtoupper(Auth::user()->name)}}</h1>
    </div>
    <div class="col-md-10 offset-md-1 dashboard-events-container">
        @if(count($events) > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Participantes</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr>
                        <td scropt="row">{{ $loop->index + 1 }}</td>
                        <td><a href="{{route("events.show", ["id"=> $event->id])}}">{{ $event->title }}</a></td>
                        <td>{{count($event->users)}}</td>
                        <td>
                            <a href="{{route("events.edit", ["event" =>  $event->id]) }}"
                               class="btn btn-info edit-btn">
                                <ion-icon name="create-outline"></ion-icon>
                                Editar</a>
                            <form onsubmit="return confirm('Esta operação não poderá ser desfeita?');"
                                  action="{{route("events.delete", ["id" =>  $event->id]) }}" method="POST"
                            >
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger delete-btn">
                                    <ion-icon name="trash-outline"></ion-icon>
                                    Deletar
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>Você ainda não tem eventos, <a href="{{route("events.created")}}">criar evento</a></p>
        @endif
    </div>
    <div class="col-md-10 offset-md-1 dashboard-title-container">
        <h1>Eventos que estou participando</h1>
    </div>

    <div class="col-md-10 offset-md-1 dashboard-events-container">
    @if(count($eventsasparticipante) > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Participantes</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($eventsasparticipante as $eventPart)
                    <tr>
                        <td scropt="row">{{ $loop->index + 1 }}</td>
                        <td><a href="{{route("events.show", ["id"=> $eventPart->id])}}">{{ $eventPart->title }}</a></td>
                        <td>{{count($eventPart->users)}}</td>
                        <td>
                            <form action="{{route("events.leaveEvente", ["event" => $eventPart->id])}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger delete-btn">
                                    <ion-icon name="trash-outline"></ion-icon>
                                    Sair do evento
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>Você não está participando de nenhum evento <a href="{{route("events")}}">ver todos</a></p>
        @endif
    </div>

@endsection
