@extends('layouts.main')

@section('title', $event->title)

@section('content')

    <div class="col-md-10 offset-md-1">
        <div class="row">
            <div id="image-container" class="col-md-6">
                @if(!empty($event->image))
{{--                    <img src="{{env("APP_URL")}}/storage/{{$event->image}}" class="img-fluid" alt="{{ $event->title }}">--}}

                    @if (strpos($event->image, 'https://via.placeholder.com') !== false)
                        <img src="{{$event->image}}" class="img-fluid" alt="{{ $event->title }}">

                    @else
                        <img src="{{env("APP_URL")}}/storage/{{$event->image}}" class="img-fluid" alt="{{ $event->title }}">

                    @endif

                @else
                    <img src="https://www.signfix.com.au/wp-content/uploads/2017/09/placeholder-600x400.png"
                         class="img-fluid" alt="{{ $event->title }}">
                @endif
            </div>
            <div id="info-container" class="col-md-6">
                <h1>{{ $event->title }}</h1>
                <p class="event-city">
                    <ion-icon name="location-outline"></ion-icon> {{ $event->city }}</p>
                <p class="events-participants">
                    <ion-icon name="people-outline"></ion-icon>
                    {{count($event->users)}} Participantes
                </p>
                <p class="event-owner">
                    <ion-icon name="star-outline"></ion-icon> {{ $dono->name }}</p>

                @if(!$hasUserJoined)
                    <form action="{{ route("events.join", ['event' => $event->id])  }}" method="POST">
                        @csrf
                        <a href="{{ route("events.join", ["event" => $event->id ]) }}"
                           class="btn btn-primary"
                           id="event-submit"
                           onclick="event.preventDefault();
                           this.closest('form').submit();"
                        >
                            Confirmar Presença
                        </a>
                    </form>
                @else
                    <p class="already-joined-msg">Você está participando desse evento!</p>
                @endif

                <h3>O evento conta com:</h3>
                <ul id="items-list">
                    @if(!empty($event->items))
                        @foreach($event->items as $item)
                            <li>
                                <ion-icon name="play-outline"></ion-icon>
                                <span>{{ $item }}</span>
                            </li>
                        @endforeach
                    @else
                        <li>
                            <p>Não foram cadastrado itens</p>
                        </li>
                    @endif

                </ul>
            </div>
            <div class="col-md-12" id="description-container">
                <h3>Sobre o evento:</h3>
                <p class="event-description">{{ $event->description }}</p>
            </div>
        </div>
    </div>

@endsection
