@extends('layouts.main')

@section('title', 'G Events programacao')

@section('content')
    <link href='{{ asset('css/main.css') }}' rel='stylesheet'/>
    <script src='{{ asset('js/main.js') }}'></script>
    <script src='{{ asset('js/pt-br.js') }}'></script>


    <input type="hidden" id="eventos" value="{{$events}}">
    <div id='calendar' style="width: 100%; display: inline-block;margin: 20px;"></div>



    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let calendarEl = document.getElementById('calendar');

            let eventos = document.getElementById('eventos').value;

            if (eventos) {
                eventos = JSON.parse(eventos)

                eventos = eventos.map(function (item) {
                    return {
                        'title': item.title,
                        'url': `http://gevents.test/events/${item.id}`,
                        'start': item.date
                    }
                })
            } else {
                eventos = []
            }


            let calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                initialDate: '2021-04-07',
                locale: 'pt-br',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                events: eventos,
                eventClick: function(event) {
                    if (event.event.url) {
                        event.jsEvent.preventDefault();
                        window.open(event.event.url, "_blank");
                    }
                }
            });

            calendar.render();
        });
    </script>


@endsection
