@extends('layouts.main')

@section('title', 'G Events')

@section('content')
    <div id="search-container" class="col-md-12">
        <h1>Busque um evento</h1>
        <form action="/" method="GET">
            <label for="search"></label><input type="text" id="search" name="search" class="form-control"
                                               placeholder="Procurar...">
        </form>
    </div>
    <div id="events-container" class="col-md-12">
        @if($search)
            <h2>Buscando por: {{ $search }}</h2>
        @else
            <h2>Próximos Eventos</h2>
            <p class="subtitle">Veja os eventos dos próximos dias</p>
        @endif
        <div id="cards-container" class="row">
            @foreach($events as $event)
                <div class="card col-md-3">
                    @if(empty($event->image))
                        <img src="https://www.signfix.com.au/wp-content/uploads/2017/09/placeholder-600x400.png"
                             alt="{{$event->title}}">
                    @else
                        @if (strpos($event->image, 'https://via.placeholder.com') !== false)
                            <img src="{{$event->image}}" alt="{{ $event->title }}">

                        @else
                            <img src="{{env("APP_URL")}}/storage/{{$event->image}}" alt="{{ $event->title }}">

                        @endif
                    @endif
                    <div class="card-body">
                        <p class="card-date">{{ date('d/m/Y', strtotime($event->date)) }}</p>
                        <h5 class="card-title">{{ $event->title }}</h5>
                        <p class="card-participants"> {{ count($event->users) }} Participantes</p>
                        <a href="{{route("events.show", ["id"=> $event->id])}}" class="btn btn-primary">Saber mais</a>
                    </div>
                </div>
            @endforeach
            @if(count($events) && $search)
                <p>Não foi possível encontrar nenhum evento com {{ $search }}! <a href="/">Ver todos</a></p>
            @elseif(count($events) == 0)
                <p>Não há eventos disponíveis</p>
            @endif
        </div>
            {{$events->render() }}
    </div>




@endsection
