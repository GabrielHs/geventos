<?php

use App\Http\Controllers\EventController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EventController::class, 'index'])->name('events');

Route::get('/events/created', [EventController::class, 'created'])->name('events.created')->middleware('auth');

Route::post('/events/created', [EventController::class, 'createdo'])->name('events.createdo');

Route::get('/events/{id}', [EventController::class, 'show'])->name('events.show')->middleware('auth');;

Route::delete('/events/{id}', [EventController::class, 'destroy'])->name('events.delete')->middleware('auth');

Route::get('/events/edit/{event}', [EventController::class, 'edit'])->name('events.edit')->middleware('auth');

Route::put('/events/edit/{event}', [EventController::class, 'editDo'])->name('events.editdo')->middleware('auth');

Route::post('/events/join/{event}', [EventController::class, 'joinEvent'])->name('events.join')->middleware('auth');

Route::delete('/events/leave/{event}', [EventController::class, 'leaveEvent'])->name('events.leaveEvente')->middleware('auth');


Route::get('/dashboard', [EventController::class, 'dashboard'])->middleware('auth')->name('dashboard');

Route::get('/programacao', [EventController::class, 'datasProgramacao'])->middleware('auth')->name('programacao');




//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//    return view('dashboard');
//})->name('dashboard');
