<?php

namespace Database\Factories;

use App\Models\Event;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
           'title' => $this->faker->name(),
            'city' => $this->faker->city(),
            'private' => $this->faker->boolean(),
            'description' => $this->faker->paragraph($nb = 1, $asText = false),
            'user_id' => User::first()->id,
            'items' => "",
            'date' => $this->faker->dateTime(),
            'image' => $this->faker->imageUrl($width = 640, $height = 480)
        ];


    }
}
